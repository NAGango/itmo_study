# Номер 7 Гостиница

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime


Base = declarative_base()


# Комнаты в наличии
class Rooms(Base):
    __tablename__ = 'Rooms'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(80), nullable=False, unique=True)
    price = sa.Column(sa.Float, nullable=False)


# Сдача комнаты
class Rent(Base):
    __tablename__ = 'Rented'

    id = sa.Column(sa.Integer, primary_key=True)
    room_name = sa.Column(
        sa.String, sa.ForeignKey(f'{Rooms.__tablename__}.name'),
    )
    days = sa.Column(sa.Float, nullable=False)
    rented_by =  sa.Column(sa.String(64), nullable=False)
    date = sa.Column(sa.Date, nullable=False)


# Менеджер офрмляющий заказ
class Rent_Managers(Base):
    __tablename__ = 'Rentmanagers'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64), nullable=False, unique=True)
    salary = sa.Column(sa.Float, nullable=False)


# Роли
class Role:
    Session = sessionmaker()

    def __init__(self, url):
        self.engine = sa.create_engine(url)
        self.session = self.Session(bind=self.engine)
        Base.metadata.create_all(self.engine)


# Роль менеджера
class RentManager(Role):
    def rent(self, name, rented_by, days):
        room = self.session.query(Rooms).filter(
            Rooms.name == name
        ).first()
        rent = Rent(
            room_name=room.name,
            rented_by=rented_by,
            days=days,
            date=datetime.now()
        )
        self.session.add(rent)
        self.session.commit()


# Роль админа
class Director_Admin(Role):
# Может задавать цены
    def change_price(self, name, price):
        room = self.session.query(Rooms).filter(
            Rooms.name == name
        ).first()
        room.name=name
        room.price=price
        self.session.commit()

# Сообщать инфу о новых свободных комнатах
    def add_room(self, name, price):
        self.session.add(
            Rooms(name=name, price=price),
        )
        self.session.commit()

# Увеличивать штат менеджеров
    def add_manager(self, name, salary):
        self.session.add(
            Rent_Managers(name=name, salary=salary)
        )
        self.session.commit()


director = Director_Admin(url='sqlite:///Hotel.db')
rentmanager = RentManager(url='sqlite:///Hotel.db')

# Сообщаем о наличии комнат и их стоимости
director.add_room(name='Аппартаменты №1', price=2500)
director.add_room(name='Аппартаменты №2', price=4000)
director.add_room(name='Аппартаменты №3', price=10000)
director.add_room(name='Аппартаменты №4', price=1600)

# Менеджеры оформляют
director.add_manager(name='Менеджер №1', salary=20000)
director.add_manager(name='Менеджер №2', salary=35000)
director.add_manager(name='Менеджер №3', salary=30000)

# Сдача комнат
rentmanager.rent(name='Аппартаменты №1', rented_by="Менеджер №2", days=24)
rentmanager.rent(name='Аппартаменты №2', rented_by="Менеджер №1", days=7)
rentmanager.rent(name='Аппартаменты №4', rented_by='Менеджер №3', days=11)

# Смена стоимости аренды Апартаментов №3, т.к. их никто не снял
director.change_price(name='Аппартаменты №3', price=8000)
