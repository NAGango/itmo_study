print("Введите номер дня недели\n1-пн 4-чт 7-вс\n2-вт 5-пт\n3-ср 6-сб\n"+"-"*15)
weekday = int(input())
while int(weekday) not in range(1, 8):
    print("Такого дня недели не бывает")
    weekday = int(input())
print("Введите данные наблюдений")
nabludenia = list(map(float, input().split()))
_1st_week = nabludenia[:7-weekday+1]  # I неделю догоняем до вс
_other_weeks = nabludenia[7-weekday+1:]  # остаток дней наблюдений


def delenie_na_nedeli(l, days):  # функция деления списка по максимум 7мь элементов
    for i in range(0, len(l), days):
        yield l[i:i + days]
days = 7

_other_weeks_1 = list(delenie_na_nedeli(_other_weeks, days))  # делим остаток по 7мь элементов
sum_1st_week = sum(_1st_week)  # суммируем данные 1й недели
sum_other = list(map(sum, _other_weeks_1))  # суммируем эл-нты внутри каждой недели
sum_other.insert(0, sum_1st_week)  # добавляем сумму за первую неделю в начало списка
print("Наибольшее количество осадков было на "+str(sum_other.index(max(sum_other))+1)+"й неделе")
c = input("Нажмите Enter для завершения")
