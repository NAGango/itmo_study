import socket
from threading import Thread


def crequest():
    while sock._closed is False:
        crequest = input()
        sock.send(crequest.encode("utf-8"))
        print(f"client > '{crequest}'")


def cresponse():
    while sock._closed is False:
        cresponse = sock.recv(1024).decode("utf-8")
        print(f"server > '{cresponse}'")


sock = socket.socket()
addr = ("127.0.0.1", 8000)
sock.connect(addr)
print(f"{addr[0]}:{addr[1]} connected")
cthread1 = Thread(target=crequest)
cthread2 = Thread(target=cresponse)
cthread1.start()
cthread2.start()
