from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from gui import Ui_MainWindow
from testserver import srequest, sresponse, sthread1, sthread2
from testclient import crequest, cresponse, cthread1, cthread2

app = QtWidgets.QApplication(sys.argv)

MainWindow = QtWidgets.QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()


def pb1():
    if ui.radioButton.isChecked():
        sthread1.start()
        sthread2.start()
    else:
        cthread1.start()
        cthread2.start()


def pb2():
    if sresponse() == True:
        ui.listWidget.addItem(sresponse())
    elif srequest() == True:
        ui.listWidget.addItem(srequest())
    elif cresponse() == True:
        ui.listWidget.addItem(cresponse())
    elif crequest() == True:
        ui.listWidget.addItem(crequest())
    else:
        print("Reload programm")


sys.exit(app.exec_())
