import socket
from threading import Thread


def srequest():
    while sock._closed is False:
        srequest = conn.recv(1024).decode("utf-8")
        print(f"client > '{srequest}'")



def sresponse():
    while sock._closed is False:
        sresponse = input()
        conn.send(sresponse.encode("utf-8"))
        print(f"server > '{sresponse}'")



sock = socket.socket()
sock.bind(("", 8000))
sock.listen(1)
conn, addr = sock.accept()
print(f"{addr[0]}:{addr[1]} connected")
sthread1 = Thread(target=srequest)
sthread2 = Thread(target=sresponse)
sthread1.start()
sthread2.start()
